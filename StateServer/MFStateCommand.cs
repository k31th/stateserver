﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace MFStateServer {
	public abstract class MFStateCommand : ICommand { 
		#region Message Event
		public event dCommandMessage IncomingMessage;

		internal void OnIncomingMessage(string message) {
			if (IncomingMessage != null) {
				IncomingMessage((ICommand)this, message);
			}
		}

		protected void SendServiceMessage(string message) {
			OnIncomingMessage(message);
		}
		#endregion

		#region ICommand
		private IService _service;

		public IService Service {
			get {
				return _service;
			}
			set {
				_service = value;
			}
		}

		private IClient _client;

		public IClient Client {
			get {
				return _client;
			}
			set {
				_client = value;
			}
		}

		private Type _requestType;

		public Type RequestType {
			get {
				return _requestType;
			}
			set {
				_requestType = value;
			}
		}

		private byte[] _requestData;

		public byte[] RequestData {
			get {
				return _requestData;
			}
			set {
				_requestData = value;
			}
		}

		private Dictionary<string, object> _response;

		public Dictionary<string, object> Response {
			get {
				if (_response == null) {
					_response = new Dictionary<string, object>();
				}

				return _response;
			}
			set {
				_response = value;
			}
		}

		private Dictionary<string, object> _parameters;

		public Dictionary<string, object> Parameters {
			get {
				if (_parameters == null) {
					_parameters = new Dictionary<string, object>();
				}

				return _parameters;
			}
			set {
				_parameters = value;
			}
		}
		#endregion

		protected Dictionary<string, object> GetErrorResponse(string error) {
			Dictionary<string, object> errorResponse = new Dictionary<string, object>();

			errorResponse.Add("error", true);
			errorResponse.Add("errorMessage", error);

			return errorResponse;
		}

		public virtual void Initialize() {
			string sJSON = Encoding.UTF8.GetString(_requestData);

			try {
				_parameters = (Dictionary<string, object>)JsonConvert.DeserializeObject(
					sJSON,
					typeof(Dictionary<string, object>)
				);
			} catch (JsonSerializationException) {
				// was prolly an empty array
				_parameters = new Dictionary<string, object>();
			}
		}

		public virtual void Dispose() {

		}

		public abstract void ProcessRequest();
	}
}

