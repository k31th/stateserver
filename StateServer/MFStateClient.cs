using System;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using MFStateServer.Services.State.Data;
using Newtonsoft.Json;

namespace MFStateServer {
	public abstract class MFStateClient : StateUser, IClient {
		#region MessageEvent
		public event dClientMessage MessageToConsole;

		internal void OnMessageToConsole(string message) {
			if (MessageToConsole != null) {
				MessageToConsole((IClient)this, message);
			}
		}

		protected void SendServiceMessage(string message) {
			OnMessageToConsole(message);
		}
		#endregion

		#region IClient
		private DateTime _lastCheckin;
		public DateTime LastCheckin {
			get {
				return _lastCheckin;
			}
			set {
				_lastCheckin = value;
			}
		}

		private MFStateServerSocketInfo _socketInfo;
		public MFStateServerSocketInfo SocketInfo {
			get {
				return _socketInfo;
			}
			set {
				_socketInfo = value;
			}
		}

		private IService _service;
		public IService Service {
			get {
				return (IService)_service;
			}
			set {
				_service = (IService)value;
			}
		}

		private Thread _clientThread;
		public Thread ClientThread {
			get {
				return _clientThread;
			}
			set {
				_clientThread = value;
			}
		}

		private bool _isRunning = false;
		public bool IsRunning {
			get {
				return _isRunning;
			}       
		}

		public abstract void Initialize();

		public void RunClient(object command) {
			try {
				ICommand xCommand = ParseCommand((byte[])command);
				ProcessCommand(xCommand);

				SendData(xCommand);
			} catch(Exception) {
				Dictionary<string, object> errorResponse = new Dictionary<string, object>();
				errorResponse.Add("error", true);
				errorResponse.Add("errorMessage", "Unauthorized Access");

				SendData(errorResponse);
			}
		}
		#endregion        

		public virtual void ProcessCommand(ICommand command) {
			command.Initialize();
			command.ProcessRequest();
			command.Dispose();

			LastCheckin = DateTime.Now;
		}

		private ICommand ParseCommand(byte[] incomingData) {
			string sIncomingRequest = Encoding.UTF8.GetString(incomingData, 0, incomingData.Length).Trim();

			StateCommandJSON xNewCommand = (StateCommandJSON)JsonConvert.DeserializeObject(
					                              sIncomingRequest,
					                              typeof(StateCommandJSON));         

			Type xServiceType = Type.GetType(xNewCommand.ServiceType);
			object xServiceObject = _service.GetLoadedService(xServiceType);          

			if (xServiceObject == null) {
				SendServiceMessage(xNewCommand.ServiceType + " failed to load");
				return null;
			}

			if (xServiceObject is IService) {
				Type xCommandType = Type.GetType(xNewCommand.RequestType);

				if (xCommandType != null) {
					ICommand xCommand = (ICommand)Activator.CreateInstance(xCommandType);
					xCommand.RequestData = Convert.FromBase64String(xNewCommand.RequestData);
					xCommand.RequestType = Type.GetType(xNewCommand.RequestType);
					xCommand.Client = _service.GetLoadedClient(Type.GetType(xNewCommand.ServiceType), UniqueKey.HasValue ? UniqueKey.Value : Guid.Empty);
					xCommand.Client.SocketInfo = this.SocketInfo;

					xCommand.Service = (IService)xServiceObject;
					xCommand.IncomingMessage += new dCommandMessage(delegate(ICommand sender, string message) {
						SendServiceMessage(message);
					});

					return xCommand;
				}
			}

			return null;
		}

		private void SendData(Dictionary<string, object> response) {
			string sJson = JsonConvert.SerializeObject(response);
			SocketInfo.OutgoingBuffer = Encoding.UTF8.GetBytes(sJson + Program.Configuration.EndOfTransmission);

			if (SocketInfo.WorkSocket.Connected) {
				SocketInfo.WorkSocket.BeginSend(SocketInfo.OutgoingBuffer,
																				0,
																				SocketInfo.OutgoingBuffer.Length,
																				SocketFlags.None,
																				new AsyncCallback(EndSendRemoveClient),
																				SocketInfo);

			}
		}

		private void EndSendRemoveClient(IAsyncResult ar) {
			MFStateServerSocketInfo xSocketInfo = (MFStateServerSocketInfo)ar.AsyncState;
			xSocketInfo.BytesSentLastRequest = xSocketInfo.WorkSocket.EndReceive(ar);
			xSocketInfo.BytesSentTotal += xSocketInfo.BytesSentLastRequest;

			try {
				xSocketInfo.WorkSocket.Disconnect(false);
				xSocketInfo.WorkSocket.Dispose();
			} catch {
			}

			Service.RemoveClient((IClient)this);
		}

		private void SendData(ICommand command) {       
			string sJson = JsonConvert.SerializeObject(command.Response);  

			SocketInfo.OutgoingBuffer = Encoding.UTF8.GetBytes(sJson + Program.Configuration.EndOfTransmission);
			SocketInfo.CurrentCommand = command;

			if (SocketInfo.WorkSocket.Connected) {
				SocketInfo.WorkSocket.BeginSend(SocketInfo.OutgoingBuffer, 
																				0, 
																				SocketInfo.OutgoingBuffer.Length, 
																				SocketFlags.None, 
																				new AsyncCallback(EndSend), 
																				SocketInfo);
			} else {
				SocketInfo.WorkSocket.Disconnect(false);
				SocketInfo.WorkSocket.Dispose();
			}
		}

		private void EndSend(IAsyncResult ar) {
			MFStateServerSocketInfo xSocketInfo = (MFStateServerSocketInfo)ar.AsyncState;
			xSocketInfo.BytesSentLastRequest = xSocketInfo.WorkSocket.EndSend(ar);
			xSocketInfo.BytesSentTotal += xSocketInfo.BytesSentLastRequest;

			if (xSocketInfo.CurrentCommand is Services.State.Commands.Disconnect) {
				try {
					xSocketInfo.WorkSocket.Disconnect(false);
					xSocketInfo.WorkSocket.Dispose();
				} catch {
				}
			} else {
				xSocketInfo.WorkSocket.BeginReceive(SocketInfo.IncomingBuffer,
																						0,
																						SocketInfo.IncomingBuffer.Length,
																						SocketFlags.None,
																						new AsyncCallback(EndReceive),
																						SocketInfo);
			}
		}

		private void EndReceive(IAsyncResult ar) {
			MFStateServerSocketInfo xSocketInfo = (MFStateServerSocketInfo)ar.AsyncState;
			xSocketInfo.BytesReceivedLastRequest = xSocketInfo.WorkSocket.EndReceive(ar);
			xSocketInfo.BytesReceivedTotal += xSocketInfo.BytesReceivedLastRequest;

			if (xSocketInfo.BytesReceivedLastRequest > 0) {
				string incomingBuffer = Encoding.UTF8.GetString(xSocketInfo.IncomingBuffer, 0, xSocketInfo.IncomingBuffer.Length);
				
				if (incomingBuffer.IndexOf(Program.Configuration.EndOfTransmission) > 0) {
					incomingBuffer = incomingBuffer.Substring(0, incomingBuffer.IndexOf(Program.Configuration.EndOfTransmission) + 1);
				}

				incomingBuffer = incomingBuffer.Trim();

				if (incomingBuffer.EndsWith(Program.Configuration.EndOfTransmission)) {
					xSocketInfo.ResetIncomingBuffer();
					xSocketInfo.StringBuffer += incomingBuffer.Replace(Program.Configuration.EndOfTransmission, string.Empty);
					string sCommand = xSocketInfo.StringBuffer;
					xSocketInfo.StringBuffer = string.Empty;

					ICommand xCommand = ParseCommand(Encoding.UTF8.GetBytes(sCommand));

					if (xCommand != null) {
						xSocketInfo.CurrentCommand.Client.ProcessCommand(xCommand);
						SendData(xCommand);
					} else {
						Dictionary<string, object> errorResponse = new Dictionary<string, object>();
						errorResponse.Add("error", true);
						errorResponse.Add("errorMessage", "Unknown command");

						SendData(errorResponse);
					}
				} else {
					xSocketInfo.StringBuffer += incomingBuffer;

					xSocketInfo.WorkSocket.BeginReceive(SocketInfo.IncomingBuffer,
																							0,
																							SocketInfo.IncomingBuffer.Length,
																							SocketFlags.None,
																							new AsyncCallback(EndReceive),
																							SocketInfo);
				}
			}
		}
	}
}