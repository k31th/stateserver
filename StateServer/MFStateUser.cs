﻿using System;
using System.Net;

namespace MFStateServer {
	public abstract class MFStateUser : IUser {
		private Guid? _uniqueKey = Guid.Empty;

		public Guid? UniqueKey {
			get {
				return _uniqueKey;
			}
			set {
				_uniqueKey = value;
			}
		}

		private string _username = string.Empty;
		public string Username {
			get {
				return _username;
			}
			set {
				_username = value;
			}
		}

		private IPAddress _clientIp;
		public IPAddress ClientIP {
			get {
				return _clientIp;
			}
			set {
				_clientIp = value;
			}
		}
	}
}