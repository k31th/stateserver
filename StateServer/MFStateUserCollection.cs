﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MFStateServer {
	public class MFStateUserCollection : CollectionBase, IEnumerable<IUser>, ICollection<IUser>, IList<IUser> {
		public bool Contains(Guid userKey) {
			foreach (IUser x in List) {
				if (x.UniqueKey.HasValue) {
					if (x.UniqueKey.Value == userKey) {
						return true;
					}
				}
			}

			return false;
		}

		public IUser this[int index] {
			get {
				return (IUser)List[index];
			}
			set {
				List[index] = value;
			}
		}

		public IUser this[Guid userKey] {
			get {
				foreach (IUser x in List) {	
					if (x.UniqueKey == userKey) {
						return x;
					}
				}

				return null;
			}
			set {
				for (int i = 0; i < List.Count; i++) {				
					if (((IUser)List[i]).UniqueKey == userKey) {
						this[i] = value;
						return;
					}
				}
			}
		}

		#region CollectionBase
		public void Add(IUser content) {
			List.Add(content);
		}
		public void Remove(int index) {
			if (index > Count - 1 || index < 0) {
				throw new Exception("Invalid Index!");
			} else {
				List.RemoveAt(index);
			}
		}
		#endregion

		#region IEnumerable
		public int IndexOf(IUser item) {
			return List.IndexOf(item);
		}
		public void Insert(int index, IUser item) {
			List.Insert(index, item);
		}
		public bool Contains(IUser item) {
			return List.Contains(item);
		}
		public void CopyTo(IUser[] array, int arrayIndex) {
			List.CopyTo(array, arrayIndex);
		}
		public bool IsReadOnly {
			get { return false; }
		}
		public bool Remove(IUser item) {
			try {
				List.Remove(item);

				return true;
			} catch {
				return false;
			}
		}
		#endregion

		#region ICollection
		void ICollection<IUser>.Add(IUser item) {
			List.Add(item);
		}

		void ICollection<IUser>.Clear() {
			List.Clear();
		}

		bool ICollection<IUser>.Contains(IUser item) {
			return List.Contains(item);
		}

		void ICollection<IUser>.CopyTo(IUser[] array, int arrayIndex) {
			List.CopyTo(array, arrayIndex);
		}

		int ICollection<IUser>.Count {
			get { return List.Count; }
		}

		bool ICollection<IUser>.IsReadOnly {
			get { return false; }
		}
		bool ICollection<IUser>.Remove(IUser item) {
			try {
				List.Remove(item);
				return true;
			} catch {
				return false;
			}
		}
		IEnumerator<IUser> IEnumerable<IUser>.GetEnumerator() {
			return (IEnumerator<IUser>)List.GetEnumerator();
		}
		#endregion
	}
}

