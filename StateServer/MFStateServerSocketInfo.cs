﻿using System;
using System.Net.Sockets;

namespace MFStateServer {
	public class MFStateServerSocketInfo {
		private const int BUFFERLENGTH = 1024;

		private Socket _workSocket;
		public Socket WorkSocket {
			get {
				return _workSocket;
			}
			set {
				_workSocket = value;
			}
		}

		private ICommand _currentCommand;
		public ICommand CurrentCommand {
			get {
				return _currentCommand;
			}
			set {
				_currentCommand = value;
			}
		}

		private long _bytesReceivedTotal;
		public long BytesReceivedTotal {
			get {
				return _bytesReceivedTotal;
			}
			set {
				_bytesReceivedTotal = value;
			}
		}

		private int _bytesSentTotal;
		public int BytesSentTotal {
			get {
				return _bytesSentTotal;
			}
			set {
				_bytesSentTotal = value;
			}
		}

		private int _bytesSentLastRequest;
		public int BytesSentLastRequest {
			get {
				return _bytesSentLastRequest;
			}
			set {
				_bytesSentLastRequest = value;
			}
		}

		private int _bytesReceivedLastRequest;
		public int BytesReceivedLastRequest {
			get {
				return _bytesReceivedLastRequest;
			}
			set {
				_bytesReceivedLastRequest = value;
			}
		}

		private byte[] _outgoingBuffer = new byte[BUFFERLENGTH];
		public byte[] OutgoingBuffer {
			get {
				return _outgoingBuffer;
			}
			set {
				_outgoingBuffer = value;
			}
		}

		private byte[] _incomingBuffer = new byte[BUFFERLENGTH];
		public byte[] IncomingBuffer {
			get {
				return _incomingBuffer;
			}
			set {
				_incomingBuffer = value;
			}
		}

		private string _stringBuffer = string.Empty;
		public string StringBuffer {
			get {
				return _stringBuffer;
			}
			set {
				_stringBuffer = value;
			}
		}

		public void ResetIncomingBuffer() {
			_incomingBuffer = new byte[BUFFERLENGTH];
		}
  }
}

