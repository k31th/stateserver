﻿using System;

namespace MFStateServer {
	[Serializable]
	public class MFStateCommandJSON : ICommandJSON {		
		private string _serviceType;

		public string ServiceType {
			get { 
				return _serviceType;
			}
			set {
				_serviceType = value;
			}
		}

		private string _requestType;

		public string RequestType {
			get {
				return _requestType;
			}
			set {
				_requestType = value;
			}
		}

		private string _requestData;

		public string RequestData {
			get {
				return _requestData;
			}
			set {
				_requestData = value;
			}			
		}

		private Guid? _stateKey;
		public Guid? StateKey {
			get {
				return _stateKey;
			}
			set {
				_stateKey = value;
			}
		}
	}
}

