using System;
using System.Linq;
using System.Threading;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Net;
using System.Text;
using MFStateServer.Services.State.Data;
using Newtonsoft.Json;

namespace MFStateServer {
	public abstract class MFStateService : IService {
		private Thread serviceThread;
		private Thread tickerThread;
		private static ManualResetEvent allDone = new ManualResetEvent(false);
		private Socket listener;	

		#region IService
		public event dIncomingServiceEvent IncomingServiceEvent;

		internal void OnIncomingEvent(string message) {
			if (IncomingServiceEvent != null) {
				IncomingServiceEvent(this, message);
			}
		}

		private long _totalBytesSent;

		public long TotalBytesSent {
			get {
				return _totalBytesSent;
			}
		}

		private long _totalBytesReceived;

		public long TotalBytesReceived {
			get {
				return _totalBytesReceived;
			}
		}

		private List<IClient> _loadedClients = new List<IClient>();

		public List<IClient> LoadedClients {
			get {
				return _loadedClients;
			}
		}

		private List<IService> _loadedServices = new List<IService>();

		public List<IService> LoadedServices {
			get {
				return _loadedServices;
			}
			set {
				_loadedServices = value;
			}
		}

		private bool _isRunning = false;

		public bool IsRunning {
			get {
				return _isRunning;
			}
			set {
				_isRunning = value;
			}
		}

		private bool _showIncomingRequests = false;
		public bool ShowIncomingRequests {
			get {
				return _showIncomingRequests;
			}
			set {
				_showIncomingRequests = value;
			}
		}

		public abstract Type ClientType {
			get;
		}

		public void StartService() {
			LoadedServices.Add((IService)this);         

			serviceThread = new Thread(new ThreadStart(runServer));
			tickerThread = new Thread(new ThreadStart(runTicker));

			_isRunning = true;         

			serviceThread.Start();
			tickerThread.Start();
		}

		public void StopService() {
			LoadedServices.Remove((IService)this);

			serviceThread.Abort();
			tickerThread.Abort();

			_isRunning = false;  
		}    

		public abstract void RemoveClient(IClient client);

		public virtual void RunServiceMaintainence() {
			var xOldClients = (from a in LoadedClients
												 where a.LastCheckin.AddMinutes(1) <= DateTime.Now 
												 select a);

			List<IClient> clientsToRemove = new List<IClient>();

			foreach (IClient xOldClient in xOldClients) {
				clientsToRemove.Add(xOldClient);
			}

			foreach (IClient xOldClient in clientsToRemove) {
				OnIncomingEvent("Removing client " + xOldClient.GetType().ToString() + " -> " + xOldClient.UniqueKey.ToString());

				foreach (IService xService in LoadedServices) {
					xService.RemoveClient(xOldClient);
				}
			}
		}
		#endregion 

		#region Threaded Functions
		private void runTicker() {
			while (_isRunning) {
				RunServiceMaintainence();
				Thread.Sleep(1000);
			}
		}

		private void runServer() {
			_totalBytesReceived = 0;
			_totalBytesSent = 0;

			IPAddress ipAddress = Program.Configuration.BindIP;
			int port = Program.Configuration.Port;

			IPEndPoint localEndPoint = new IPEndPoint(
				ipAddress,
				port
			);

			listener = new Socket(
				AddressFamily.InterNetwork,
				SocketType.Stream,
				ProtocolType.Tcp
			);

			try {
				listener.Bind(localEndPoint);
				listener.Listen(100);

				while (_isRunning) {
					allDone.Reset();

					listener.BeginAccept(new AsyncCallback(acceptCallback), listener);

					allDone.WaitOne();
				}
			} catch(Exception ex) {
				Console.WriteLine(ex.Message);
			}         
		}	
		#endregion

		#region Loaded Services / Clients
		public IEnumerable<IClient> GetLoadedClients(Type clientType) {
			foreach (IClient xClient in LoadedClients) {
				if (xClient.GetType() == clientType) {
					yield return xClient;
				}
			}
		}

		public IService GetLoadedService(Type serviceType) {
			IService loadedService;

			foreach (IService xService in LoadedServices) {
				if (xService.GetType() == serviceType) {
					return xService;
				}
			}

			loadedService = (IService)Activator.CreateInstance(serviceType);
			LoadedServices.Add(loadedService);

			return loadedService;
		}

		public IClient GetLoadedClient(Type serviceType, Guid uniqueKey) {
			/* FIND ORIGINAL INSTANCE OF CLIENT IF ANY */
			if (uniqueKey != Guid.Empty) {
				foreach (IClient xClient in LoadedClients) {
					if (xClient.UniqueKey == uniqueKey) {
						Type currentService = xClient.Service.GetType();

						if (currentService == serviceType) {
							return xClient;
						}
					}
				}
			}

			/* LOAD INSTANCE OF SERVICE */
			IService xService = (IService)Activator.CreateInstance(serviceType);

			/* GET SERVICE CLIENT TYPE */
			IClient xClientt = (IClient)Activator.CreateInstance(xService.ClientType);

			if (uniqueKey == Guid.Empty) {
				uniqueKey = Guid.NewGuid();

				OnIncomingEvent("Issued new service key: " + uniqueKey.ToString());
			}

			/* APPLY PROPERTIES TO CLIENT */
			xClientt.Service = GetLoadedService(serviceType);
			xClientt.UniqueKey = uniqueKey;
			xClientt.LastCheckin = DateTime.Now;

			xClientt.MessageToConsole += new dClientMessage(delegate(IClient client, string message) {
				OnIncomingEvent(message);
			});

			_loadedClients.Add(xClientt);
			xClientt.Initialize();

			return xClientt;
		}
		#endregion

		#region Socket Communication
		private void acceptCallback(IAsyncResult ar) {
			allDone.Set();

			/* ACCEPT NEW CONNECTION */
			Socket listener = (Socket)ar.AsyncState;
			Socket handler = listener.EndAccept(ar);

			MFStateServerSocketInfo xSocketInfo = new MFStateServerSocketInfo();
			xSocketInfo.WorkSocket = handler;

			xSocketInfo.WorkSocket.BeginReceive(xSocketInfo.IncomingBuffer, 
																				  0, 
																				  xSocketInfo.IncomingBuffer.Length, 
																				  SocketFlags.None, 
																				  new AsyncCallback(endReceive),
																					xSocketInfo);
		}
		private void endReceive(IAsyncResult ar) {
			MFStateServerSocketInfo xSocketInfo = (MFStateServerSocketInfo)ar.AsyncState;

			xSocketInfo.BytesReceivedLastRequest = xSocketInfo.WorkSocket.EndReceive(ar);
			xSocketInfo.BytesReceivedTotal += xSocketInfo.BytesReceivedLastRequest;
			_totalBytesReceived += xSocketInfo.BytesReceivedLastRequest;

			if (xSocketInfo.BytesReceivedLastRequest > 0) {
				string incomingData = Encoding.UTF8.GetString(xSocketInfo.IncomingBuffer, 0, xSocketInfo.BytesReceivedLastRequest);

				if (incomingData.EndsWith(Program.Configuration.EndOfTransmission)) {
					xSocketInfo.ResetIncomingBuffer();
					xSocketInfo.StringBuffer += incomingData.Replace(Program.Configuration.EndOfTransmission, string.Empty);
					string sCommand = xSocketInfo.StringBuffer;
					xSocketInfo.StringBuffer = string.Empty;

					if (_showIncomingRequests) {
						OnIncomingEvent(sCommand);
					}

					MFStateCommandJSON xNewCommand = (MFStateCommandJSON)JsonConvert.DeserializeObject(
						sCommand,
						typeof(MFStateCommandJSON)
					); 

					if (xNewCommand != null) {
						IClient xNewClient = GetLoadedClient(Type.GetType(xNewCommand.ServiceType), xNewCommand.StateKey.HasValue ? xNewCommand.StateKey.Value : Guid.Empty);
						xNewClient.SocketInfo = xSocketInfo;

						ParameterizedThreadStart xParameterTS = new ParameterizedThreadStart(xNewClient.RunClient);
						xNewClient.ClientThread = new Thread(xParameterTS);
						xNewClient.ClientThread.Start(Encoding.UTF8.GetBytes(sCommand));
					} else {
						xSocketInfo.WorkSocket.Dispose();
					}
				} else {
					xSocketInfo.StringBuffer += incomingData;

					xSocketInfo.WorkSocket.BeginReceive(xSocketInfo.IncomingBuffer,
																							0,
																							xSocketInfo.IncomingBuffer.Length,
																							SocketFlags.None,
																							new AsyncCallback(endReceive),
																							xSocketInfo);
				}
			}
		}
		#endregion
	}
}