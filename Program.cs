﻿using System;
using System.Collections.Generic;
using System.Threading;

using MFStateServer.Services.State;

namespace MFStateServer {
	class Program {
		private static bool daemonMode = false;
		private static int tickThreshold = 15000;
		private static StateService stateService;
		private static Configuration currentConfiguration = new Configuration();
		private static List<string> outputBuffer = new List<string>();
		private static List<string> commandHistory = new List<string>();
		private static List<char> incomingCommand = new List<char>();
		private static object waitLock = new object();
		private static bool messageRefreshToggle = true;
		private static Thread serviceMaintenceThread = null;
    
		public static Configuration Configuration {
			get {
				return currentConfiguration;
			}
			set {
				currentConfiguration = value;
			}
		}

		private static bool GetOnOff(string onoff) {
			return GetOnOff(onoff, true);
		}
		private static bool GetOnOff(string onoff, bool defaultVal) {
			bool onOff;

			if (onoff == "on") {
				onoff = "true";
			} else if (onoff == "off") {
				onoff = "false";
			}

			if (!bool.TryParse(onoff, out onOff)) {
				onOff = defaultVal;
			}

			return onOff;
		}

		private static void MainProgramLoop() {
			int historyLocation = 0;

			while (stateService.IsRunning) {
				if (daemonMode) {
					Thread.Sleep(tickThreshold);
				} else {
					ConsoleKeyInfo xInput = Console.ReadKey();

					if (xInput.Key == ConsoleKey.Backspace) {
					
						if (incomingCommand.Count > 0) {
							incomingCommand.RemoveAt(incomingCommand.Count - 1);
						}
					} else if (xInput.Key == ConsoleKey.UpArrow) {
						historyLocation--;

						if (historyLocation < 0) {
							historyLocation = 0;
						}

						if (commandHistory.Count > historyLocation) {
							incomingCommand = new List<char>(commandHistory[historyLocation]);
						}
					} else if (xInput.Key == ConsoleKey.DownArrow) {
						historyLocation++;

						if (historyLocation >= commandHistory.Count) {
							incomingCommand = new List<char>();
							historyLocation = commandHistory.Count;
						} else {
							incomingCommand = new List<char>(commandHistory[historyLocation]);
						}
					} else if (xInput.Key == ConsoleKey.Enter) {
						string command = new string(incomingCommand.ToArray());

						commandHistory.Add(command);
						historyLocation = commandHistory.Count;

						string[] commandArgs = command.Split(' ');

						if (commandArgs.Length > 0) {
							switch (commandArgs[0].ToLower()) {
								case "quit":
									ExitProgram();
									break;

								case "refresh":
									RefreshAll();
									break;

								case "option":
									if (commandArgs.Length > 1) {
										switch (commandArgs[1].ToLower()) {
											case "requests":
												stateService.ShowIncomingRequests = GetOnOff(commandArgs[2].ToLower(), false);
												outputBuffer.AddRange(chopString("Turned Incoming Requests " + (stateService.ShowIncomingRequests ? "On" : "Off")));
												break;
											case "messages":
												messageRefreshToggle = GetOnOff(commandArgs[2].ToLower());
												outputBuffer.AddRange(chopString("Turned messages " + (messageRefreshToggle ? "On" : "Off")));
												break;
										}
									}
									break;

							
								case "show":

									if (commandArgs.Length > 1) {
										switch (commandArgs[1].ToLower()) {
											case "clients":
												if (stateService.LoadedClients.Count > 0) {
													foreach (IClient client in stateService.LoadedClients) {
														outputBuffer.AddRange(chopString(client.UniqueKey.ToString() + " -> " + client.GetType().ToString()));
													}
												} else {
													outputBuffer.AddRange(chopString("No connected clients"));
												}
												break;
											case "services":
												if (stateService.LoadedServices.Count > 0) {
													foreach (IService service in stateService.LoadedServices) {
														outputBuffer.AddRange(chopString(service.GetType().ToString()));
													}
												} else {
													outputBuffer.AddRange(chopString("No running services"));
												}
												break;
										}
									} else {
										outputBuffer.AddRange(chopString("Invalid arguments to the command show"));
									}
									break;
							}
						}

						RefreshHeading();
						RefreshMessageBuffer();

						incomingCommand.Clear();
					} else if (xInput.Key != ConsoleKey.Enter) {
						incomingCommand.Add(xInput.KeyChar);
					}

					RefreshHeading();
					RefreshInputArea();
				}
			}
		}    
		private static void ServiceMaintainanceLoop() {
			while (stateService.IsRunning) {
				foreach (IService xService in stateService.LoadedServices) {
					xService.RunServiceMaintainence();
				}

				Thread.Sleep(15000);
			}
		}

		/* ENTRY */
		private static void Main(string[] args) {
			Console.BackgroundColor = ConsoleColor.Black;
			Console.ForegroundColor = ConsoleColor.White;
			Console.CancelKeyPress += HandleCancelKeyPress;      
          
			RefreshHeading();
			RefreshMessageBuffer();
			RefreshInputArea();		
      
			foreach (string sArg in args) {
				if (sArg.ToLower().Trim() == "-d") {
					daemonMode = true;
				}
			}

			stateService = new StateService();
			stateService.IncomingServiceEvent += HandleIncomingEvent;
			stateService.StartService();

			serviceMaintenceThread = new Thread(new ThreadStart(ServiceMaintainanceLoop));
			serviceMaintenceThread.Start();

			MainProgramLoop();

			stateService.StopService();
			Program.ExitProgram();
		}
    
		/* EXIT */
		private static void ExitProgram() {
			serviceMaintenceThread.Abort();

			stateService.IsRunning = false; 
		}

    #region Console Refresh Functions
		private static void OutputFullHorizontal(char firstChar) {
			OutputFullHorizontal(firstChar, firstChar); 
		}

		private static void OutputFullHorizontal(char firstChar, char altChar) {
			int iLeft = 0;
			bool alt = false;
      
			while (iLeft < Console.WindowWidth) {
				if (alt) {
					Console.Write(firstChar);
				} else {
					Console.Write(altChar);
				}
        
				alt = !alt;
				iLeft++;
			}
		}

		private static List<string> chopString(string sString) {
			int maxWidth = Console.WindowWidth - 1;
			int outputWidth = sString.Length;

			List<string> realOutputLines = new List<string>();

			if (outputWidth > maxWidth) {
				char[] allOutput = sString.ToCharArray();
				string substring = string.Empty;
				int iwide = 0;

				for (int ai = 0; ai < sString.Length; ai++) {
					if (iwide < maxWidth) {
						substring += allOutput[ai];
						iwide++;
					} else {
						iwide = 0;
						realOutputLines.Add(substring);
						substring = string.Empty;
					}
				}

				realOutputLines.Add(substring);
			} else {
				realOutputLines.Add(sString);
			}

			return realOutputLines;
		}
    
		private static void OutputString(string sString, int row) {
			char[] realOutputString = sString.ToCharArray();
			int fromWidth = sString.Length;

			while (fromWidth < Console.WindowWidth) {
				Console.SetCursorPosition(fromWidth++, row);
				Console.Write(' '); 
			}

			for (int i = realOutputString.Length - 1; i >= 0; i--) {
				Console.SetCursorPosition(i, row);
				Console.Write(realOutputString[i]);
			}
		}
    
		private static void RefreshHeading() {
			lock(waitLock) {
				Console.SetCursorPosition(0, 0);      
				OutputString("MFStateServer", 0);
				Console.SetCursorPosition(0, 1);
				OutputFullHorizontal('-');      
			}
		}
    
		private static void RefreshMessageBuffer() {
			lock(waitLock) {
				int fromHeight = Console.WindowHeight - 3;
				int toHeight = 2;
				int iMessageCounter = outputBuffer.Count - 1;
    
				while (fromHeight >= toHeight) {
					Console.SetCursorPosition(0, fromHeight);
      
					if (iMessageCounter >= 0) {
						OutputString(outputBuffer[iMessageCounter], fromHeight);
						iMessageCounter--;
					} else {
						OutputFullHorizontal(' ');
					}
      
					fromHeight--;
				}      
			}
		}
    
		private static void RefreshInputArea() {
			lock(waitLock) {
				Console.SetCursorPosition(0, Console.WindowHeight - 2);
				OutputFullHorizontal('-');
				Console.SetCursorPosition(0, Console.WindowHeight - 1);
				OutputFullHorizontal(' ');
				Console.SetCursorPosition(0, Console.WindowHeight - 1);

				string prompt = string.IsNullOrEmpty(Configuration.Prompt) ? "-=> " : Configuration.Prompt + " ";
				int iWidth = (Console.WindowWidth - prompt.Length) - 1;
				int iCommandStart = 0;

				if (incomingCommand.Count > iWidth) {
					iCommandStart = (incomingCommand.Count - iWidth);
				}

				Console.Write(prompt);

				while (iCommandStart < incomingCommand.Count) {
					Console.Write(incomingCommand[iCommandStart++]);
				}
			}
		}

		private static void RefreshAll() {
			RefreshHeading();
			RefreshMessageBuffer();
			RefreshInputArea();
		}
    #endregion
    
    #region Console Events
		private static void HandleCancelKeyPress(object sender, ConsoleCancelEventArgs e) {      
			ExitProgram();
		}
    #endregion
    
    #region State Server Events
		private static void HandleIncomingEvent(IService server, string message) {
			if (messageRefreshToggle) {
				string sOutput = DateTime.Now.ToString() + ": " + message;

				foreach (string sChopped in chopString(sOutput)) {
					outputBuffer.Add(sChopped);
				}

				RefreshAll();
			}
		}
    #endregion
	}
}
