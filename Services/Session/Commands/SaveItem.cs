﻿using System;

namespace MFStateServer.Services.Session.Commands {
	public class SaveItem : SessionCommand {
		public override void ProcessRequest() {
			SessionClient xClient = (SessionClient)Client;

			try {
				if (!string.IsNullOrEmpty((string)Parameters["key"])) {
					if (xClient.SessionItems.Contains((string)Parameters["key"])) {
						Response.Add((string)Parameters["key"], xClient.SessionItems[(string)Parameters["key"]].Value);
					} else {
						Response.Add((string)Parameters["key"], null);
					}

					Response.Add("error", false);
				} else {
					Response.Add("error", false);
					Response.Add((string)Parameters["key"], null);
				}
			} catch (Exception) {
				Response.Add("error", true);
			}
		}
  }
}

