﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MFStateServer {
	public class SessionItemCollection : CollectionBase, IEnumerable<SessionItem>, ICollection<SessionItem>, IList<SessionItem> {
		public bool Contains(string key) {
			foreach (SessionItem x in List) {
				if (x.Key.ToLower() == key.ToLower()) {
					return true;
				}
			}

			return false;
		}

		public SessionItem this[int index] {
			get {
				return (SessionItem)List[index];
			}
			set {
				List[index] = value;
			}
		}

		public SessionItem this[string key] {
			get {
				foreach (SessionItem x in List) {	
					if (x.Key.ToLower() == key.ToLower()) {
						return x;
					}
				}

				return null;
			}
			set {
				for (int i = 0; i < List.Count; i++) {				
					if (((SessionItem)List[i]).Key.ToLower() == key.ToLower()) {
						this[i] = value;
						return;
					}
				}
			}
		}

		#region CollectionBase
		public void Add(SessionItem content) {
			List.Add(content);
		}
		public void Remove(int index) {
			if (index > Count - 1 || index < 0) {
				throw new Exception("Invalid Index!");
			} else {
				List.RemoveAt(index);
			}
		}
		#endregion

		#region IEnumerable
		public int IndexOf(SessionItem item) {
			return List.IndexOf(item);
		}
		public void Insert(int index, SessionItem item) {
			List.Insert(index, item);
		}
		public bool Contains(SessionItem item) {
			return List.Contains(item);
		}
		public void CopyTo(SessionItem[] array, int arrayIndex) {
			List.CopyTo(array, arrayIndex);
		}
		public bool IsReadOnly {
			get { return false; }
		}
		public bool Remove(SessionItem item) {
			try {
				List.Remove(item);

				return true;
			} catch {
				return false;
			}
		}
		#endregion

		#region ICollection
		void ICollection<SessionItem>.Add(SessionItem item) {
			List.Add(item);
		}

		void ICollection<SessionItem>.Clear() {
			List.Clear();
		}

		bool ICollection<SessionItem>.Contains(SessionItem item) {
			return List.Contains(item);
		}

		void ICollection<SessionItem>.CopyTo(SessionItem[] array, int arrayIndex) {
			List.CopyTo(array, arrayIndex);
		}

		int ICollection<SessionItem>.Count {
			get { return List.Count; }
		}

		bool ICollection<SessionItem>.IsReadOnly {
			get { return false; }
		}
		bool ICollection<SessionItem>.Remove(SessionItem item) {
			try {
				List.Remove(item);
				return true;
			} catch {
				return false;
			}
		}
		IEnumerator<SessionItem> IEnumerable<SessionItem>.GetEnumerator() {
			return (IEnumerator<SessionItem>)List.GetEnumerator();
		}
		#endregion
  }
}

