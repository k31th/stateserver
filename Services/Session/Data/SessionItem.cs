﻿using System;
using System.Collections.Generic;

namespace MFStateServer {
  public class SessionItem {
		private object _value;
		public object Value {
			get {
				return _value;
			}
			set {
				_value = value;
			}
		}

		private string _key;
		public string Key {
			get {
				return _key;
			}
			set {
				_key = value;
			}
		}

		private Type _type;
		public Type Type {
			get {
				return _type;
			}
			set {
				_type = value;
			}
		}

		public SessionItem(string key, object value, Type valueType) {
			_key = key;
			_value = value;
			_type = valueType;
		}
  }
}

