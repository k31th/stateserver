﻿using System;
using System.Collections.Generic;
using MFStateServer.Services.State;

namespace MFStateServer.Services.Session {
	public class SessionClient : MFStateClient {
		private SessionItemCollection _sessionItems;
		public SessionItemCollection SessionItems {
			get {
				if (_sessionItems == null) {
					_sessionItems = new SessionItemCollection();
				}

				return _sessionItems;
			}
			set {
				_sessionItems = value;
			}
		}

		public override void Initialize() {

		}
  }
}

