﻿using System;
using MFStateServer.Services.Chat.Data;

namespace MFStateServer.Services.Chat.Responses {
	[Serializable]
  public class GetChannelUpdatesResponse {
		private enChatEventType _eventType;
		public enChatEventType EventType {
			get {
				return _eventType;
			}
			set {
				_eventType = value;
			}
		}

		private string _message = null;
		public string Message {
			get {
				return _message;
			}
			set {
				_message = value;
			}
		}

		private string _username = null;
		public string Username {
			get {
				return _username;
			}
			set {
				_username = value;
			}
		}

    public GetChannelUpdatesResponse() {
    }
  }
}

