﻿using MFStateServer.Services.Chat.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MFStateServer.Services.Chat.Data {
	public class ChatChannel {
		private ChatUserCollection _users = new ChatUserCollection();
		public ChatUserCollection Users {
			get {
				return _users;
			}
		}

		private string _channelName;
		public string ChannelName {
			get {
				return _channelName;
			}
			set {
				_channelName = value;
			}
		}

		private List<ChatEvent> _events;
		public List<ChatEvent> Events {
			get {
				if (_events == null) {
					_events = new List<ChatEvent>();
				}

				return _events;
			}
			set {
				_events = value;
			}
		}

		public void RemoveOldMessages() {
			var xMessages = (from a in Events
				where a.LogTime.AddMinutes(Program.Configuration.ExpiredMessageMinute <= 0 ? 15 : Program.Configuration.ExpiredMessageMinute) <= DateTime.Now
			                 select a).ToList();

			foreach (ChatEvent xEvent in xMessages) {
				Events.Remove(xEvent);
			}
		}

		public List<ChatEvent> GetUnseenEvents(IUser user) {
			List<ChatEvent> unseenEvents = new List<ChatEvent>();

			foreach (ChatEvent channelEvent in Events) {
				if (!channelEvent.UsersSeen.Contains(user)) {
					unseenEvents.Add(channelEvent);
					channelEvent.UsersSeen.Add(user);
				}
			}

			return (from a in unseenEvents
							orderby a.LogTime ascending
							select a).ToList();
		}

		public bool AddUser(IUser newUser) {
			if (newUser.UniqueKey.HasValue) {
				if (!_users.Contains(newUser.UniqueKey.Value)) {
					_users.Add(newUser);

					ChatEvent xNewChatEvent = new ChatEvent();
					xNewChatEvent.LogTime = DateTime.Now;
					xNewChatEvent.EventType = enChatEventType.JoinChannel;
					xNewChatEvent.User = newUser;

					Events.Add(xNewChatEvent);

					return true;
				}
			}

			return false;
		}

		public void AddMessage(IUser user, string message) {
			if (user.UniqueKey.HasValue) {
				ChatEvent xNewChatEvent = new ChatEvent();
				xNewChatEvent.LogTime = DateTime.Now;
				xNewChatEvent.EventType = enChatEventType.PublicMessage;
				xNewChatEvent.User = user;
				xNewChatEvent.Message = new ChatMessage() {
					Message = message,
					SendDate = xNewChatEvent.LogTime,
					Sender = xNewChatEvent.User
				};

				xNewChatEvent.UsersSeen.Add(user);

				Events.Add(xNewChatEvent);
			}
		}

		public void RemoveUser(IUser user) {
			if (_users.Remove(user)) {
				ChatEvent xNewChatEvent = new ChatEvent();
				xNewChatEvent.LogTime = DateTime.Now;
				xNewChatEvent.EventType = enChatEventType.LeaveChannel;
				xNewChatEvent.User = user;

				Events.Add(xNewChatEvent);
			}
		}

		/*
		private List<ChatEvent> _channelEvents = new List<ChatEvent>();
		public List<ChatEvent> ChannelEvents {
			get {
				return _channelEvents;
			}
			set {
				_channelEvents = value;
			}
		}

		private List<ChatMessage> _currentMessages = new List<ChatMessage>();
		public List<ChatMessage> CurrentMessages {
			get {
				return _currentMessages;
			}
			set {
				_currentMessages = value;
			}
		}

		public List<ChatEvent> GetUnseenEvents(ChatUser user, DateTime lastTick) {
			var xRet = (from a in ChannelEvents
									where a.LogTime >= lastTick && !a.UsersSeen.Contains(user)
									orderby a.LogTime ascending
									select a).ToList();

			return xRet;
		}

		public void RegisterUser(IUser user) {
			if (!_currentUsers.Contains(user)) {
				_currentUsers.Add(user);
				
				_channelEvents.Add(new ChatEvent() {
					User = user,
					LogTime = DateTime.Now,
					EventType = enChatEventType.JoinChannel
				});
			}
		}

		public void UnregisterUser(string user) {
			if (!string.IsNullOrEmpty(user)) {
				var x = (from a in _currentUsers
								 where a.Username == user
								 select a).SingleOrDefault();

				if (x != null) {
					UnregisterUser(x);
				}
			}
		}
		public void UnregisterUser(ChatUser user) {
			_currentUsers.Remove(user);
			_channelEvents.Add(new ChatEvent() {
				User = user,
				LogTime = DateTime.Now,
				EventType = enChatEventType.LeaveChannel
			});
		}

		public List<ChatMessage> GetUnreadMessages(ChatUser user) {
			List<ChatMessage> unreadMessages = new List<ChatMessage>();

			foreach (ChatMessage unreadMessage in CurrentMessages) {
				if (!unreadMessage.ReadBy.Contains(user) && unreadMessage.Sender != user) {
					unreadMessage.ReadBy.Add(user);
					unreadMessages.Add(unreadMessage);
				}
			}

			return (from a in unreadMessages
							orderby a.SendDate ascending
							select a).ToList();
		}
		*/
	}
}
