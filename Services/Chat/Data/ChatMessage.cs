﻿using System;
using System.Collections.Generic;

namespace MFStateServer.Services.Chat.Data {
	public class ChatMessage {
		private string _message;
		public string Message {
			get {
				return _message;
			}
			set {
				_message = value;
			}
		}

		private DateTime? _sendDate;
		public DateTime? SendDate {
			get {
				return _sendDate;
			}
			set {
				_sendDate = value;
			}
		}

		private IUser _sender;
		public IUser Sender {
			get {
				return _sender;
			}
			set {
				_sender = value;
			}
		}
	}
}
