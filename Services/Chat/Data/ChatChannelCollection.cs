using System;
using System.Collections;
using MFStateServer.Services.Chat;
using System.Collections.Generic;

namespace MFStateServer.Services.Chat.Data {
  public class ChatChannelCollection : CollectionBase, IEnumerable<ChatChannel>, ICollection<ChatChannel>, IList<ChatChannel> {
		public bool Contains(string channel) {
			foreach (ChatChannel x in List) {
				if (x.ChannelName.ToLower().Trim() == channel.ToLower().Trim()) {
					return true;
				}
			}

			return false;
		}

		public bool ChannelContainsUser(string channel, Guid userKey) {
			ChatChannel x = this[channel];

			if (x != null) {
				foreach (IUser xUser in x.Users) {

				}
			}

			return false;
		}

		public ChatChannel this[int index] {
			get {
				return (ChatChannel)List[index];
			}
			set {
				List[index] = value;
			}
		}

		public ChatChannel this[string channelName] {
			get {
				foreach (ChatChannel x in List) {	
					if (x.ChannelName.Trim().ToLower() == channelName.Trim().ToLower()) {
						return x;
					}
				}

				return null;
			}
			set {
				for (int i = 0; i < List.Count; i++) {				
					if (((ChatChannel)List[i]).ChannelName.ToLower().Trim() == channelName.Trim().ToLower()) {
						this[i] = value;
						return;
					}
				}
			}
		}

    #region CollectionBase
		public void Add(ChatChannel content) {
			List.Add(content);
		}
		public void Remove(int index) {
			if (index > Count - 1 || index < 0) {
				throw new Exception("Invalid Index!");
			} else {
				List.RemoveAt(index);
			}
		}
		#endregion

		#region IEnumerable
		public int IndexOf(ChatChannel item) {
			return List.IndexOf(item);
		}
		public void Insert(int index, ChatChannel item) {
			List.Insert(index, item);
		}
		public bool Contains(ChatChannel item) {
			return List.Contains(item);
		}
		public void CopyTo(ChatChannel[] array, int arrayIndex) {
			List.CopyTo(array, arrayIndex);
		}
		public bool IsReadOnly {
			get { return false; }
		}
		public bool Remove(ChatChannel item) {
			try {
				List.Remove(item);

				return true;
			} catch {
				return false;
			}
		}
		#endregion

		#region ICollection
		void ICollection<ChatChannel>.Add(ChatChannel item) {
			List.Add(item);
		}

		void ICollection<ChatChannel>.Clear() {
			List.Clear();
		}

		bool ICollection<ChatChannel>.Contains(ChatChannel item) {
			return List.Contains(item);
		}

		void ICollection<ChatChannel>.CopyTo(ChatChannel[] array, int arrayIndex) {
			List.CopyTo(array, arrayIndex);
		}

		int ICollection<ChatChannel>.Count {
			get { return List.Count; }
		}

		bool ICollection<ChatChannel>.IsReadOnly {
			get { return false; }
		}
		bool ICollection<ChatChannel>.Remove(ChatChannel item) {
			try {
				List.Remove(item);
				return true;
			} catch {
				return false;
			}
		}
		IEnumerator<ChatChannel> IEnumerable<ChatChannel>.GetEnumerator() {
			return (IEnumerator<ChatChannel>)List.GetEnumerator();
		}
		#endregion
  }
}

