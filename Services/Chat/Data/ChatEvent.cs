﻿using MFStateServer.Services.Chat.Data;
using System;
using System.Collections.Generic;

namespace MFStateServer.Services.Chat.Data {
	public enum enChatEventType {
		JoinChannel = 0,
		LeaveChannel = 1,
		PublicMessage = 2,
		PrivateMessage = 3
	};

	public class ChatEvent {
		private enChatEventType _eventType;
		public enChatEventType EventType {
			get {
				return _eventType;
			}
			set {
				_eventType = value;
			}
		}

		private IUser _user;
		public IUser User {
			get {
				return _user;
			}
			set {
				_user = value;
			}
		}

		private ChatMessage _message = null;
		public ChatMessage Message {
			get {
				return _message;
			}
			set {
				_message = value;
			}
		}

		private List<IUser> _usersSeen = new List<IUser>();
		public List<IUser> UsersSeen {
			get {
				return _usersSeen;
			}
			set {
				_usersSeen = value;
			}
		}

		private DateTime _logTime;
		public DateTime LogTime {
			get {
				return _logTime;
			}
			set {
				_logTime = value;
			}
		}

		public ChatEvent() {

		}
	}
}
