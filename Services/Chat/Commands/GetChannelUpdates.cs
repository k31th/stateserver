﻿using System;
using MFStateServer.Services.Chat.Data;
using MFStateServer.Services.Chat.Responses;
using System.Collections.Generic;

namespace MFStateServer.Services.Chat.Commands {
	public class GetChannelUpdates : ChatCommand {
		public override void ProcessRequest() {
			try {
				string channel = (string)Parameters["channel"];

				ChatService loadedService = (ChatService)Service;

				if (loadedService.ChatChannels.Contains(channel)) {
					ChatChannel theChannel = loadedService.ChatChannels[channel];
					List<GetChannelUpdatesResponse> response = new List<GetChannelUpdatesResponse>();

					List<ChatEvent> unseen = theChannel.GetUnseenEvents((IUser)Client);

					foreach (ChatEvent xEvent in unseen) {
						GetChannelUpdatesResponse poopie = new GetChannelUpdatesResponse() {
							EventType = xEvent.EventType,
							Username = xEvent.User.Username
						};

						if (xEvent.Message != null) {
							poopie.Message = string.IsNullOrEmpty(xEvent.Message.Message) ? null : xEvent.Message.Message;
						}

						response.Add(poopie);
					}

					SendServiceMessage("Getting unread messages for " + ((IUser)Client).UniqueKey.ToString() + ", found " + response.Count.ToString() + ".");

					Response.Add("unseenEvents", response.ToArray());
					Response.Add("error", false);
				}
			} catch (Exception ex) {
				Response.Add("error", true);
				Response.Add("errorMessage", ex.Message);
			}
		}
  }
}

