﻿using MFStateServer.Services.Chat.Data;
using System.Collections.Generic;
using System;

namespace MFStateServer.Services.Chat.Commands {
	public class GetUsersInChannel : ChatCommand {
		public override void ProcessRequest() {
			try {
				string channel = (string)Parameters["channel"];

				ChatService loadedService = (ChatService)Service;

				if (loadedService.ChatChannels.Contains(channel)) {
					ChatChannel theChannel = loadedService.ChatChannels[channel];

					List<string> currentUsers = new List<string>();

					foreach (IUser xUser in theChannel.Users) {
						currentUsers.Add(xUser.Username);
					}

					Response.Add("users", currentUsers.ToArray());
				} else {
					Response.Add("users", new string[] { });
				}

				Response.Add("error", false);
			} catch (Exception) {
				Response.Add("error", true);
			}
		}
	}
}