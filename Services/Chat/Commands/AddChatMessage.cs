﻿using System;
using MFStateServer.Services.Chat.Data;
using MFStateServer.Services.Chat.Responses;
using System.Collections.Generic;

namespace MFStateServer.Services.Chat.Commands {
	public class AddChatMessage : ChatCommand {
		public override void ProcessRequest() {
			string channel = (string)Parameters["channel"];

			ChatService loadedService = (ChatService)Service;

			if (loadedService.ChatChannels.Contains(channel)) {
				ChatChannel theChannel = loadedService.ChatChannels[channel];

				theChannel.AddMessage((IUser)Client, (string)Parameters["message"]);

				SendServiceMessage(channel + ", " + ((IUser)Client).Username + ": " + (string)Parameters["message"]);

				Response.Add("error", false);
			} else {
				Response.Add("error", true);
			}
		}
  }
}

