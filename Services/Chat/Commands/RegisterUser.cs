﻿using MFStateServer.Services.Chat.Data;
using System;

namespace MFStateServer.Services.Chat.Commands {
	public class RegisterUser : ChatCommand {
      
		public override void ProcessRequest() {		
			/* SEE IF THERE IS A CHANNEL ALREADY IF NOT CREATE IT */
			string channel = (string)Parameters["channel"];

			ChatService loadedService = (ChatService)Service;

			if (loadedService.AddChannel(channel)) {
				SendServiceMessage("Channel created " + channel);
			}

			/* GET CHANNEL */
			ChatChannel destinationChannel = loadedService.GetChannel(channel);

			if (destinationChannel != null) {
				/* IS CLIENT IN CHANNEL ALREADY */
				ChatClient loadedClient = (ChatClient)Client;
				loadedClient.Username = (string)Parameters["username"];

				if (!loadedClient.Channels.Contains(channel)) {
					if (destinationChannel.AddUser((IUser)Client)) {
						if (!loadedClient.Channels.Contains(destinationChannel.ChannelName)) {
							loadedClient.Channels.Add(destinationChannel);

							SendServiceMessage("Added user " + ((IUser)Client).Username + " to channel " + channel);
						}
					}
				}
			}
		}
	}
}
