﻿using System;
using MFStateServer.Services.Chat.Data;
using MFStateServer.Services.State;

namespace MFStateServer.Services.Chat {
	public class ChatClient : MFStateClient {
		private ChatChannelCollection _channels;
		public ChatChannelCollection Channels {
			get {
				if (_channels == null) {
					_channels = new ChatChannelCollection();
				}

				return _channels;
			}
			set {
				_channels = value;
			}		
		}

		public override void Initialize() {

		}


	}
}
