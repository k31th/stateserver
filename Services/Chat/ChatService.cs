﻿using System.Collections.Generic;
using System;
using System.Linq;
using MFStateServer.Services.Chat.Data;

namespace MFStateServer.Services.Chat {
	public class ChatService : MFStateService {
		private const int defaultScrollBackAmount = 1000;
		private ChatChannelCollection _chatChannels;

		public ChatChannelCollection ChatChannels {
			get {
				if (_chatChannels == null) {
					_chatChannels = new ChatChannelCollection();
				}

				return _chatChannels;
			}
		}

		public override Type ClientType {
			get {
				return typeof(ChatClient);
			}
		}

		public override void RunServiceMaintainence() {
			foreach (ChatChannel xChannel in ChatChannels) {
				xChannel.RemoveOldMessages();
			}

			base.RunServiceMaintainence();
		}

		public void RemoveUserFromChannels(IUser user) {
			List<string> channelsToRemove = new List<string>();

			foreach (ChatChannel xChannel in ChatChannels) {
				if (xChannel.Users.Contains(user.UniqueKey.Value)) {
					channelsToRemove.Add(xChannel.ChannelName);
				}
			}

			foreach (string channelToRemove in channelsToRemove) {
				ChatChannels[channelToRemove].RemoveUser(user);
			}
		}

		public override void RemoveClient(IClient client) {
			RemoveUserFromChannels((IUser)client);
		}

		public bool AddChannel(string channelName) {
			if (!ChatChannels.Contains(channelName)) {
				ChatChannel channel = new ChatChannel() {
					ChannelName = channelName         
				};

				ChatChannels.Add(channel);

				return true;
			}

			return false;
		}

		public ChatChannel GetChannel(string channelName) {
			if (ChatChannels.Contains(channelName)) {
				return ChatChannels[channelName];
			}

			return null;
		}
	}
}
