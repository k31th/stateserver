﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using MFStateServer.Services.State.Data;
using Newtonsoft.Json;

namespace MFStateServer.Services.State {
	public class StateService : MFStateService {	
		public override Type ClientType {
			get {
				return typeof(StateClient);
			}
		}

		public override void RemoveClient(IClient client) {
			if (client.ClientThread != null) {
				client.ClientThread.Abort();
			}

			LoadedClients.Remove(client);
		}
	}
}
