using System;
using System.Net;
using MFStateServer.Services.State;
using MFStateServer.Services.State.Data;

namespace MFStateServer.Services.State.Commands {
  public class Connect : StateCommand {
    public override void ProcessRequest() {
			Response.Add("error", false);
			Response.Add("newkey", Client.UniqueKey.Value.ToString());

			string clientIp = (string)Parameters["clientip"];
			IPAddress testAddress;

			if (IPAddress.TryParse(clientIp, out testAddress)) {
				((IUser)Client).ClientIP = testAddress;
			}

			SendServiceMessage("Client connected [" + ((IUser)Client).ClientIP.ToString() + "]");
    }
  }
}

