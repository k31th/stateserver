﻿using System;
using MFStateServer.Services.State;
using MFStateServer.Services.State.Data;

namespace MFStateServer.Services.State.Commands {
	public class Disconnect : StateCommand {
		public override void ProcessRequest() {
			Response.Add("error", false);

			SendServiceMessage("Client disconnected [" + ((IUser)Client).ClientIP.ToString() + "]");
		}
	}
}
