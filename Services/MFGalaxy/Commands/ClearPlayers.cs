﻿namespace MFStateServer.Services.MFGalaxy.Commands {
	public class ClearPlayers : MFGalaxyCommand {
		public override void ProcessRequest() {
			MFGalaxyService galaxyService = (MFGalaxyService)Service;

			try {
				galaxyService.OnlinePlayers.Clear();
			} catch {
				Response.Add("error", "true");
			}

			Response.Add("error", "false");
		}
	}
}
