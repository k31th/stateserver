﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MFStateServer.Services.MFGalaxy.Commands {
	public class PlayerDisconnected : MFGalaxyCommand {
		public override void ProcessRequest() {
			MFGalaxyService galaxyService = (MFGalaxyService)Service;
			string currentPlayer = string.Empty;

			if (Parameters.ContainsKey("playerName")) {
				currentPlayer = Parameters["playerName"].ToString();
			}

			if (!string.IsNullOrEmpty(currentPlayer)) {
				var galaxyPlayer = (from a in galaxyService.OnlinePlayers
														where a.PlayerName == currentPlayer
														select a).FirstOrDefault();

				if (galaxyPlayer != null) {
					galaxyService.OnlinePlayers.Remove(galaxyPlayer);
					Response.Add("removed", true);
				} else {
					Response.Add("removed", false);
				}

				Response.Add("error", "false");
			} else {
				Response.Add("error", "true");
				Response.Add("removed", false);
			}
		}
	}
}
