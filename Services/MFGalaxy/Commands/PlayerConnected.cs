﻿using MFStateServer.Services.MFGalaxy.Data;
using System.Linq;
namespace MFStateServer.Services.MFGalaxy.Commands {
	public class PlayerConnected : MFGalaxyCommand {

		public override void ProcessRequest() {
			MFGalaxyService galaxyService = (MFGalaxyService)Service;

			string currentPlayer = string.Empty;
			int deaths = 0;
			int kills = 0;

			if (Parameters.Keys.Contains("playerName")) {
				currentPlayer = Parameters["playerName"].ToString();
			}

			if (Parameters.Keys.Contains("deaths")) {
				if (!int.TryParse(Parameters["deaths"].ToString(), out deaths)) {
					deaths = 0;
				}
			}

			if (Parameters.Keys.Contains("kills")) {
				if (!int.TryParse(Parameters["kills"].ToString(), out kills)) {
					kills = 0;
				}
			}

			if (!string.IsNullOrEmpty(currentPlayer)) {
				var galaxyPlayer = (from a in galaxyService.OnlinePlayers
														where a.PlayerName == currentPlayer
														select a).FirstOrDefault();

				if (galaxyPlayer == null) {
					MFGalaxyPlayer newPlayer = new MFGalaxyPlayer();
					newPlayer.PlayerName = currentPlayer;
					newPlayer.Kills = kills;
					newPlayer.Deaths = deaths;

					galaxyService.OnlinePlayers.Add(newPlayer);
					Response.Add("added", true);
				} else {
					Response.Add("added", false);
				}

				Response.Add("error", "false");
			} else {
				Response.Add("error", "true");
				Response.Add("added", false);
			}
		}
	}
}
