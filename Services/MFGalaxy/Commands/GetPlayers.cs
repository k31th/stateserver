﻿using MFStateServer.Services.MFGalaxy.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MFStateServer.Services.MFGalaxy.Commands {
	public class GetPlayers : MFGalaxyCommand {
		public override void ProcessRequest() {
			MFGalaxyService galaxyService = (MFGalaxyService)Service;
			string xStr = string.Empty;
			char sep = ((char)36);
			char rSep = ((char)35);

			foreach (MFGalaxyPlayer xPlayer in galaxyService.OnlinePlayers) {
				xStr += xPlayer.PlayerName + sep + xPlayer.Deaths.ToString() + sep + xPlayer.Kills.ToString() + rSep;
			}

			if (!string.IsNullOrEmpty(xStr)) {
				Response.Add("players", xStr.Substring(0, xStr.Length - 1));
			} else {
				Response.Add("players", string.Empty);
			}
			
			Response.Add("error", "false");
		}
	}
}
