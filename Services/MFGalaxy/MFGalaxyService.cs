﻿using MFStateServer.Services.MFGalaxy.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MFStateServer.Services.MFGalaxy {
	public class MFGalaxyService : MFStateService {
		private List<MFGalaxyPlayer> _onlinePlayers = new List<MFGalaxyPlayer>();

		public List<MFGalaxyPlayer> OnlinePlayers {
			get {
				return _onlinePlayers;
			}
			set {
				_onlinePlayers = value;
			}
		}

		public override Type ClientType {
			get {
				return typeof(MFGalaxyClient);
			}			
		}		

		public override void RemoveClient(IClient client) {
			
		}
	}
}
