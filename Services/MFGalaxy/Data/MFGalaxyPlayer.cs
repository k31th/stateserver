﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MFStateServer.Services.MFGalaxy.Data {
	public class MFGalaxyPlayer {
		public string PlayerName {
			get;
			set;
		}

		public int Kills {
			get;
			set;
		}

		public int Deaths {
			get;
			set;
		}
	}
}
