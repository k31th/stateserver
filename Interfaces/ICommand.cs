using System;
using System.Collections.Generic;

namespace MFStateServer {
	public delegate void dCommandMessage(ICommand command, string message);

	public interface ICommand {
		IService Service {
			get;
			set;
		}
      
		byte[] RequestData {
			get;
			set;
		}
      
		Dictionary<string, object> Response {
			get;
			set;
		}
      
		Dictionary<string, object> Parameters {
			get;
			set;
		}
      
		Type RequestType {
			get;
			set;
		}

		IClient Client {
			get;
			set;
		}

		event dCommandMessage IncomingMessage;      

		void Initialize();
		void ProcessRequest();
		void Dispose();
	}
}

