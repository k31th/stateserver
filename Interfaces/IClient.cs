using System;
using System.Net.Sockets;
using System.Threading;

namespace MFStateServer {
	public delegate void dClientMessage(IClient sender, string message);

	public interface IClient {
		MFStateServerSocketInfo SocketInfo {
			get;
			set;
		}
      
		IService Service {
			get;
			set;
		}
      
		Guid? UniqueKey {
			get;
			set;
		}
      
		Thread ClientThread {
			get;
			set;
		}
      
		bool IsRunning {
			get;
		}

		DateTime LastCheckin {
			get;
			set;
		}

		event dClientMessage MessageToConsole;

		void Initialize();
		void ProcessCommand(ICommand command);
		void RunClient(object incomingData);
	}
}

