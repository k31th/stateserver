using System;
using System.Collections.Generic;

namespace MFStateServer {
	public delegate void dIncomingServiceEvent (IService service, string message);
	
	public interface IService {
		event dIncomingServiceEvent IncomingServiceEvent;
		
		long TotalBytesReceived {
			get;			
		}
		
		long TotalBytesSent {
			get;			
		}
		
		List<IClient> LoadedClients {
			get;
		}
      
    List<IService> LoadedServices {
       get;
       set;
    }

		Type ClientType {
			get;
		}
		
		bool IsRunning {
			get;
			set;
		}
      
    void StartService();
    void StopService();
		void RunServiceMaintainence();
		IClient GetLoadedClient(Type serviceType, Guid uniqueKey);
    IService GetLoadedService(Type serviceType);
    void RemoveClient(IClient client);
	}
}

