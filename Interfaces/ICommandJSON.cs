using System;

namespace MFStateServer {
   public interface ICommandJSON {
      string ServiceType {
         get;
         set;
      }
      
      string RequestType {
         get;
         set;
      }
      
      string RequestData {
         get;
         set;
      }
   }
}

