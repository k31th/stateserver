using System;
using System.Net;

namespace MFStateServer {
	public interface IUser {
		Guid? UniqueKey {
			get;
			set;
		}

		string Username {
			get;
			set;
		}

		IPAddress ClientIP {
			get;
			set;
		}
	}
}

