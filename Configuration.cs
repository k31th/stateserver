using System;
using System.Net;
using System.IO;

namespace MFStateServer {
	public class Configuration {
		public string EndOfTransmission {
			get {
				return ((char)4).ToString();
			}
		}

		private IPAddress _bindIp;
		public IPAddress BindIP {
			get {
				return _bindIp; 
			}
			set {
				_bindIp = value;
			}
		}
    
		private int _port;
		public int Port {
			get {
				return _port;
			}
			set {
				_port = value;
			}
		}

		private int _expiredMessageMinute;
		public int ExpiredMessageMinute {
			get {
				return _expiredMessageMinute;
			}
			set {
				_expiredMessageMinute = value;
			}
		}

		private string _prompt;
		public string Prompt {
			get {
				return _prompt;
			}
			set {
				_prompt = value;
			}
		}

		private void ReadFile() {
			string confFile = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar.ToString() + "mfstateserver.conf";
      
			if (File.Exists(confFile)) {
				string[] sLines = File.ReadAllLines(confFile);
        
				foreach (string line in sLines) {
					if (!line.StartsWith("#")) {
						int eq = line.IndexOf('=');
						string[] x = new string[2];

						x[0] = line.Substring(0, eq - 1).Trim();
						x[1] = line.Substring(eq + 1).Trim();
            
						if (x.Length > 1) {
							switch (x[0].Trim().ToLower()) {
								case "port":
									int port;
	                
									if (int.TryParse(x[1].Trim(), out port)) {
										_port = port;
									}                
									break;
								case "expiredmessageminute":
									int emm;

									if (int.TryParse(x[1].Trim(), out emm)) {
										_expiredMessageMinute = emm;
									}
									break;
								case "prompt":
									_prompt = x[1];
									break;
								case "bindip":
									IPAddress ipAddress;                
                
									if (IPAddress.TryParse(x[1].Trim(), out ipAddress)) {
										_bindIp = ipAddress; 
									}
									break;                
							}
						}
					}
				}
			}      
		}
    
		public Configuration() {
			ReadFile();
		}
	}
}

